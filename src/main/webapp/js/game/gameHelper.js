$(document).ready(function () {


    $('#multiuserType').click(function () {
        $('#multiuserTypeOption').removeClass('hideBlock');
        $('#rateOption').addClass('hideBlock');
    });

    $('#oppponentOption').css({'margin':'20px 0 0 10%','font-size':'20pt'});
    $('#ownOption').css({'margin':'20px 0 0 10%','font-size':'20pt'});
    $('#gameResult').find('span').css({'font-size' : '20pt'});
    $('#errorMessage').css({'font-size':'16pt'});

    $('#serverType').click(function () {
        $('#multiuserTypeOption').addClass('hideBlock');
        $('.container').addClass('hideBlock');
        $('#rateOption').removeClass('hideBlock');
    });


    $('#existingMultiuserGame').click(function () {
        $('.container').removeClass('hideBlock');
        $('#rateOption').addClass('hideBlock');
    });

    $('#newMultiuserGame').click(function () {
        $('.container').addClass('hideBlock');
        $('#rateOption').removeClass('hideBlock');
    });


    $('#newGameButton').click(function () {
        $('#gameResult').empty();
        $('#dice').empty();
        $('#gameAction').addClass('hideBlock');
        $('#gameMenuId').removeClass('hideBlock');
        $('#oppponentOption').addClass('hideBlock');
        $('#ownOption').addClass('hideBlock');
        $('#ownResult').empty();
        $('#opponentResult').empty();
    });

    doRoll = function (throwArray) {
        var i,
            faceValue,
            output = '';
        for (i = 0; i < throwArray.length; i++) {
            faceValue = throwArray[i] - 1;
            output += "&#x268" + faceValue + "; ";
        }
        $('#dice').html(output);
    }

});

function validateData() {
    console.log("validate");
    $('#errorMessage').empty();
    var type = $('input[name=gameTypeRadio]:checked', '#selectGameType').val();
    var minValue = 0.01;
    if (type == 'SERVER') {
        console.log("Server");
        if ($('#inputRate').val() >= minValue) {
            $('#gameMenuId').addClass('hideBlock');
            $('#oppponentOption').removeClass('hideBlock');
            sendAjax();
        }else {
            $('#errorMessage').append('<label>Incorrect rate</label>');
        }
    } else {
        if (type == 'MULTIUSER') {
            var multiType = $('input[name=multiuserGameOptionRadio]:checked', '#multiuserTypeOption').val();
            if (multiType == 'NEW') {
                console.log("new");
                if ($('#inputRate').val() >= minValue) {
                    $('#gameMenuId').addClass('hideBlock');
                    sendAjax();
                }else {
                    $('#errorMessage').append('<label>Incorrect rate</label>');
                }
            } else {
                console.log("exist");
                if ($('.chooseGameId').prop("checked", true)) {
                    $('#gameMenuId').addClass('hideBlock');
                    $('#oppponentOption').removeClass('hideBlock');
                    sendAjax();
                }
            }
        }
    }
}

function sendAjax() {
    var gameRequest = {};
    gameRequest.requestType = "GAME";
    gameRequest.gameType = $('input[name=gameTypeRadio]:checked', '#selectGameType').val();
    gameRequest.gameId = $('input[name=chooseGameOptionRadio]:checked', '.possibleGame').val();
    gameRequest.multiuserGameType = $('input[name=multiuserGameOptionRadio]:checked', '#multiuserTypeOption').val();
    gameRequest.rate = $('#inputRate').val();


    $.ajax({
        url: "/game",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(gameRequest),

        success: function (data) {
            console.log(data);
            $('#errorMessage').empty();
            $('#gameAction').removeClass('hideBlock');
            var userConsignment = data.consignment;
            var opponentScore = data.opponentMaxScore;
            var score = data.maxScore;
            $('#opponentResult').append(opponentScore);
            $('#dice').css("font-size", "150px");
            var k = 0;
            $(document).on('click', '#rollButton', function () {
                if (k < userConsignment.length) {
                    doRoll(userConsignment[k]);
                    k++;
                }
                if (k == userConsignment.length) {
                    var winner = data.winner;
                    console.log("winn = " + winner);
                    $('#ownOption').removeClass('hideBlock');
                    $('#ownResult').append(score);
                    if (gameRequest.gameType == 'MULTIUSER' && gameRequest.multiuserGameType == 'NEW') {
                        $('#gameResult').append('<h2 id="resultWait">Your turn is over.</h2>');
                    }else {
                        if (winner == true) {
                            $('#gameResult').append('<h2 id="resultWin">You win!!!</h2>');
                        } else {
                            $('#gameResult').append('<h2 id="resultFail">Try one more time!</h2>');
                        }
                    }
                    winner = undefined;
                    k=undefined;
                }
            });

        },
        error: function (status, message) {
            $('#errorMessage').append('<label>Try later</label>');
            $('#gameResult').empty();
            $('#dice').empty();
            $('#gameAction').addClass('hideBlock');
            $('#gameMenuId').removeClass('hideBlock');
            $('#oppponentOption').addClass('hideBlock');
            $('#ownOption').addClass('hideBlock');
            $('#ownResult').empty();
            $('#opponentResult').empty();
            console.log("qwerty");
            console.log("error status: " + status.toString() + message);
        }
    });

    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function () {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = roundToTwo(oldValue + 0.01);
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function () {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = roundToTwo(oldValue - 0.01);
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });
    });
    function roundToTwo(num) {
        return +(Math.round(num + "e+2") + "e-2");
    }


}


function getPossibleGames() {

    var gameRequest = new Object();
    gameRequest.requestType = "SHOWPOSSIBLE";

    $.ajax({
        url: "/game",
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(gameRequest),

        success: function (data) {
            $('.table tbody').empty();
            $('#errorMessage').empty();
            console.log(data);
            var trHTML = '';
            $.each(data, function (key, item) {
                trHTML +=
                    '<tr><td>' + item.gameId +
                    '</td><td>' + item.userLogin +
                    '</td><td>' + item.rate +
                    '</td><td>' + '<div class="funkyradio">' +
                    '<div class="funkyradio-success">' +
                    '<input type="radio" class="chooseGameId" name="chooseGameOptionRadio" value="' + item.gameId + '" id="gameId' + item.gameId + '">' +
                    '<label style="margin: 0;padding-right: 10px;" for="gameId' + item.gameId + '">Join!</label>' +
                    '</div>' +
                    '</div>' +
                    '</td></tr>';
            });

            $('.possibleGame tbody').append(trHTML);

        },
        error: function (status) {
            var code = status;
            console.log("error status: " + code.toString());
            $('#errorMessage').append('<label>Try later</label>');
            $('#gameResult').empty();
            $('#dice').empty();
            $('#gameAction').addClass('hideBlock');
            $('#gameMenuId').removeClass('hideBlock');
            $('#oppponentOption').addClass('hideBlock');
            $('#ownOption').addClass('hideBlock');
            $('#ownResult').empty();
            $('#opponentResult').empty();
        }
    });
}