<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${sessionScope.userLocale}"/>
<fmt:bundle basename="pagecontent">
    <html>
    <head>
        <meta charset="utf-8">
        <title>Game</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/checkboxStyle.css">
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/game/game.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/footerStyle.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/js/listHelper.js"></script>
        <script src="${pageContext.servletContext.contextPath}/js/game/gameHelper.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    </head>
    <body>
    <h1>Lucky Dice</h1>
    <div id="errorMessage"></div>
    <div class="gameMenu" id="gameMenuId">
        <div id="gameRequest">
            <div class="form-inline">
                <button class="btn btn-lg btn-danger" id="playButton" type="button" onclick="validateData()">Play!
                </button>
                <div class="form-group form-inline" id="rateOption">
                    <label for="inputRate" class="control-label">Rate: </label>
                    <input class="form-control" type="number" id="inputRate" min="0" step="0.01" value="0.00">
                </div>
            </div>

            <div class="funkyradio form-inline" id="selectGameType">
                <div class="funkyradio-primary">
                    <input type="radio" name="gameTypeRadio" value="SERVER" checked id="serverType"/>
                    <label for="serverType">Server</label>
                </div>
                <div class="funkyradio-success">
                    <input type="radio" name="gameTypeRadio" value="MULTIUSER" id="multiuserType"/>
                    <label for="multiuserType">Multiuser</label>
                </div>
            </div>

            <div class="funkyradio hideBlock form-inline" id="multiuserTypeOption">
                <div class="funkyradio-warning">
                    <input type="radio" name="multiuserGameOptionRadio" value="NEW" id="newMultiuserGame"/>
                    <label for="newMultiuserGame">New Game</label>
                </div>
                <div class="funkyradio-warning">
                    <input type="radio" name="multiuserGameOptionRadio" onclick="getPossibleGames()" value="EXISTING"
                           id="existingMultiuserGame"/>
                    <label for="existingMultiuserGame">Existing Game</label>
                </div>
            </div>
        </div>

        <a class="btn btn-info" href="${pageContext.servletContext.contextPath}/jsp/common/main.jsp">Home</a>

        <div class="container hideBlock">
            <h2>Open games</h2>
            <table id="table1" class="possibleGame table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Game Id</th>
                    <th>Login</th>
                    <th>Rate</th>
                    <th>Join</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

    </div>

    <div id="gameAction" class="form-inline hideBlock">
        <input id="rollButton" class="btn btn-info" type="button" value="Roll">
        <input id="newGameButton" class="btn btn-success" type="button" value="New Game">
        <div id="oppponentOption" class="form-inline hideBlock">
            <label>Opponent : </label>
            <span id="opponentResult"></span>
        </div>
        <div id="ownOption" class="form-inline hideBlock">
            <label>Score : </label>
            <span id="ownResult"></span>
        </div>
        <div id="dice"></div>
    </div>


    <div id="gameResult">

    </div>
    <div id="footer">
        <ctg:info-tag/>
    </div>
    </body>
    </html>
</fmt:bundle>