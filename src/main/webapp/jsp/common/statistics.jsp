<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${sessionScope.userLocale}"/>
<fmt:bundle basename="pagecontent">
    <html>
<head>
    <title><fmt:message key="statistics"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/menu.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/statisticStyle.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/diceStyle.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/footerStyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<c:set var="visitor" value="${sessionScope.visitor}"/>
<c:choose>
    <c:when test="${visitor == 'ADMIN'}">
        <c:import url="/jsp/admin/jsp/header.jsp"/>
    </c:when>
    <c:otherwise>
        <c:import url="/jsp/user/jsp/header.jsp"/>
    </c:otherwise>
</c:choose>
<div id="statisticInfo">
    <div id="allGame" class="form-inline">
        <label><fmt:message key="all_game"/> </label>
        <span>${game}</span>
    </div>
    <div id="winGame" class="form-inline">
        <label><fmt:message key="win_game"/> </label>
        <span>${winGame}</span>
    </div>
</div>
<div>
    <div class="containerTwo">
        <div class="back side">
            <div class="dot center"></div>
        </div>
        <div class="left side">
            <div class="dot dtop dleft"></div>
            <div class="dot dbottom dright"></div>
        </div>
        <div class="right side">
            <div class="dot dtop dleft"></div>
            <div class="dot center"></div>
            <div class="dot dbottom dright"></div>
        </div>
        <div class="top side">
            <div class="dot dtop dleft"></div>
            <div class="dot dtop dright"></div>
            <div class="dot dbottom dleft"></div>
            <div class="dot dbottom dright"></div>
        </div>
        <div class="bottom side">
            <div class="dot center"></div>
            <div class="dot dtop dleft"></div>
            <div class="dot dtop dright"></div>
            <div class="dot dbottom dleft"></div>
            <div class="dot dbottom dright"></div>
        </div>
        <div class="front side">
            <div class="dot dtop dleft"></div>
            <div class="dot dtop dright"></div>
            <div class="dot dbottom dleft"></div>
            <div class="dot dbottom dright"></div>
            <div class="dot center dleft"></div>
            <div class="dot center dright"></div>
        </div>
    </div>
</div>
<div id="footer">
    <ctg:info-tag/>
</div>
</body>
</html>
</fmt:bundle>