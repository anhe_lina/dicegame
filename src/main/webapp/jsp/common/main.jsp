<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${sessionScope.userLocale}"/>
<fmt:bundle basename="pagecontent">
<html>
<head>
    <title><fmt:message key="home"/></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/menu.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/diceStyle.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/mainStyle.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/footerStyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<c:set var="visitor" value="${sessionScope.visitor}"/>
<c:choose>
    <c:when test="${visitor == 'ADMIN'}">
        <c:import url="/jsp/admin/jsp/header.jsp"/>
    </c:when>
    <c:otherwise>
        <c:import url="/jsp/user/jsp/header.jsp"/>
    </c:otherwise>
</c:choose>
<label id="welcome">${sessionScope.username} <fmt:message key="welcome_text"/></label>
<form action="${pageContext.servletContext.contextPath}/controller" method="GET">
    <input type="hidden" name="command" value="showstatistics"/>
    <input type="submit" class="btn btn-warning" id="showStatistic" value="<fmt:message key="show_statistic"/>">
</form>
<br/>
<div id="errorMessage" onload="showError()">
    <p><fmt:message key="${errorMainMessage}"/></p>
</div>
<br/>
<div id="rules">
    <fmt:message key="rules"/>
</div>
<div>
    <div class="container">
        <div class="back side">
            <div class="dot center"></div>
        </div>
        <div class="left side">
            <div class="dot dtop dleft"></div>
            <div class="dot dbottom dright"></div>
        </div>
        <div class="right side">
            <div class="dot dtop dleft"></div>
            <div class="dot center"></div>
            <div class="dot dbottom dright"></div>
        </div>
        <div class="top side">
            <div class="dot dtop dleft"></div>
            <div class="dot dtop dright"></div>
            <div class="dot dbottom dleft"></div>
            <div class="dot dbottom dright"></div>
        </div>
        <div class="bottom side">
            <div class="dot center"></div>
            <div class="dot dtop dleft"></div>
            <div class="dot dtop dright"></div>
            <div class="dot dbottom dleft"></div>
            <div class="dot dbottom dright"></div>
        </div>
        <div class="front side">
            <div class="dot dtop dleft"></div>
            <div class="dot dtop dright"></div>
            <div class="dot dbottom dleft"></div>
            <div class="dot dbottom dright"></div>
            <div class="dot center dleft"></div>
            <div class="dot center dright"></div>
        </div>
    </div>
</div>
<div>
    <div class="containerTwo">
        <div class="back side">
            <div class="dot center"></div>
        </div>
        <div class="left side">
            <div class="dot dtop dleft"></div>
            <div class="dot dbottom dright"></div>
        </div>
        <div class="right side">
            <div class="dot dtop dleft"></div>
            <div class="dot center"></div>
            <div class="dot dbottom dright"></div>
        </div>
        <div class="top side">
            <div class="dot dtop dleft"></div>
            <div class="dot dtop dright"></div>
            <div class="dot dbottom dleft"></div>
            <div class="dot dbottom dright"></div>
        </div>
        <div class="bottom side">
            <div class="dot center"></div>
            <div class="dot dtop dleft"></div>
            <div class="dot dtop dright"></div>
            <div class="dot dbottom dleft"></div>
            <div class="dot dbottom dright"></div>
        </div>
        <div class="front side">
            <div class="dot dtop dleft"></div>
            <div class="dot dtop dright"></div>
            <div class="dot dbottom dleft"></div>
            <div class="dot dbottom dright"></div>
            <div class="dot center dleft"></div>
            <div class="dot center dright"></div>
        </div>
    </div>
</div>
<div id="footer">
    <ctg:info-tag/>
</div>
<script>
    $(document).ready(function () { $("div p:contains('??????')").parent('div').hide(); });
</script>
</body>
</html>
</fmt:bundle>