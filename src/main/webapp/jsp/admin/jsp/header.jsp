<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.userLocale}"/>
<fmt:bundle basename="pagecontent">
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body class="menuContent">
    <form action="${pageContext.servletContext.contextPath}/controller" method="GET" id="showCreditCardAccount" style="display: none;">
        <input type="hidden" name="command" value="showcreditaccount"/>
        <input type="submit" name="submit" value="showcreditaccount" id="showAccount">
    </form><nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${pageContext.servletContext.contextPath}/jsp/common/main.jsp"><fmt:message key="brand"/></a>
                <div class="pull-right flag">
                    <form action="${pageContext.servletContext.contextPath}/controller" method="GET" class="wrap">
                        <input type="hidden" name="command" value="locale"/>
                        <input type="text" value="ru-RU" name="locale"  class="localeLabel">
                        <input type="submit" name="submit" value="" class="button-localeRU">
                    </form>
                </div>
                <div class="pull-right flag">
                    <form action="${pageContext.servletContext.contextPath}/controller" method="GET" class="wrap">
                        <input type="hidden" name="command" value="locale"/>
                        <input type="text" value="en-EN" name="locale" class="localeLabel">
                        <input type="submit" name="submit" value="" class="button-localeEN">
                    </form>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="menuItem"><a href="${pageContext.servletContext.contextPath}/jsp/common/main.jsp"><fmt:message key="home"/></a></li>
                    <li class="menuItem"><a href=""><label for="showAccount"><span class="glyphicon glyphicon-piggy-bank"></span> ${sessionScope.userScore}</label></a></li>
                    <li class="menuItem"><a href="${pageContext.servletContext.contextPath}/jsp/admin/creditCardSetting.jsp"><fmt:message key="settings"/></a></li>
                    <li class="menuItem"><a href="${pageContext.servletContext.contextPath}/jsp/common/game/gameMain.jsp"><fmt:message key="menu_game"/></a></li>
                    <li class="menuItem"><a href="${pageContext.servletContext.contextPath}/jsp/common/mail/newMail.jsp"><fmt:message key="menu_mail"/></a></li>
                    <li class="menuItem"><a href="${pageContext.servletContext.contextPath}/jsp/common/profileMain.jsp"><fmt:message key="profile"/></a></li>
                    <li class="menuItem"><a href="${pageContext.servletContext.contextPath}/controller?command=logout&submit=logout"><span class="glyphicon glyphicon-log-out"></span> <fmt:message key="logout"/></a></li>
                </ul>
            </div>
        </div>
    </nav>
    </body>
    </html>
</fmt:bundle>