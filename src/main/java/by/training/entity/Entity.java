package by.training.entity;

import java.io.Serializable;

/**
 * The super class for {@link by.training.servlet.DiceServlet} and {@link by.training.servlet.DiceGame} entities.
 *
 * @author Maretskaya Anhelina
 * @see Cloneable
 * @see Serializable
 */
public abstract class Entity implements Cloneable, Serializable {
}