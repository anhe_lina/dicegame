package by.training.servlet;

import by.training.command.ActionCommand;
import by.training.command.ActionFactory;
import by.training.entity.response.ResponseInfo;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The class provides controller for client requests at MVC pattern of application.
 *
 * @author Maretskaya Anhelina
 * @see HttpServlet
 */
@WebServlet(name = "DiceServlet", urlPatterns = {"/controller"})
@MultipartConfig
public class DiceServlet extends HttpServlet {

    /**
     * Processes request sent by POST method.
     *
     * @param request  request from client to get parameters
     * @param response response to client with parameters to work with on client side
     * @throws IOException      if an input or output error is detected when the servlet handles the request
     * @throws ServletException if the request could not be handled
     * @see HttpServletRequest
     * @see HttpServletResponse
     * @see #processRequest(HttpServletRequest, HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Processes request sent by GET method.
     *
     * @param request  request from client to get parameters
     * @param response response to client with parameters to work with on client side
     * @throws IOException      if an input or output error is detected when the servlet handles the request
     * @throws ServletException if the request could not be handled
     * @see HttpServletRequest
     * @see HttpServletResponse
     * @see #processRequest(HttpServletRequest, HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Processes requests.
     * Defines command name by {@link HttpServletRequest#getAttribute(String)} and directs request to
     * {@link ActionFactory}.
     * {@link ActionCommand#execute(HttpServletRequest)} {@link ResponseInfo} object.
     * If error occurs user navigates to error page.
     *
     * @param request  request from client to get parameters to work with
     * @param response response to client with parameters to work with on client side
     * @throws IOException      if an input or output error is detected when the servlet handles the request
     * @throws ServletException if the request could not be handled
     * @see ActionFactory
     * @see by.training.command.CommandType
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(request);
        ResponseInfo responseInfo = command.execute(request);
        switch (responseInfo.getType()) {
            case FORWARD: {
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(responseInfo.getNextPage());
                dispatcher.forward(request, response);
                break;
            }
            case REDIRECT: {
                response.sendRedirect(responseInfo.getNextPage());
                break;
            }
        }
    }
}