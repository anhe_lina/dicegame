package by.training.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Class provides custom tag with developers' name and email link for user questions.
 *
 * @author Maretskaya Anhelina
 */
@SuppressWarnings("serial")
public class InfoTag extends TagSupport {
    private static final String INFO_TEXT = "&copy; Anhelina Maretskaya, 2017, FAQ <a target=\"_blank\" href=\"mailto:anhelina.maretskaya@gmail.com?subject=Lucky%20Dice&amp;body=Lucky%20Dice&amp;\">anhelina.maretskaya@gmail.com</a>";

    @Override
    public int doStartTag() throws JspException {
        try {
            pageContext.getOut().write(INFO_TEXT);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}