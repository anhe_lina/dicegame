package by.training.command;

import by.training.entity.response.ResponseInfo;

import javax.servlet.http.HttpServletRequest;

/**
 * Common interface for Commands suitable to use with {@link by.training.servlet.DiceServlet}.
 *
 * @author Maretskaya Anhelina
 */
public interface ActionCommand {
    /**
     * Executes definite operation with data parsed from request and returns {@link ResponseInfo} data.
     *
     * @param request - request from client to get parameters to work with
     * @return {@link ResponseInfo} with parameters next page and response type
     */
    ResponseInfo execute(HttpServletRequest request);
}
