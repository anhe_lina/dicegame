package by.training.command.game;

import by.training.logic.game.GameWithServerLogic;
import by.training.entity.game.GameRequest;
import by.training.entity.game.GameResponse;
import by.training.entity.game.type.GameType;
import by.training.logic.game.GameWithUserLogic;
import by.training.exception.CommandException;

/**
 * Prepare response according to game type
 *
 * @author Maretskaya Anhelina
 */
public class GameCommand {
    /**
     * Manage game types
     *
     * @param gameRequest - {@link GameRequest} from ajax Json to work with
     * @return {@link GameResponse} with generated game parameters
     * @throws CommandException
     */
    public GameResponse execute(GameRequest gameRequest) throws CommandException {
        GameResponse gameResponse = null;
        GameType gameType = gameRequest.getGameType();
        switch (gameType) {
            case SERVER: {
                gameResponse = GameWithServerLogic.play(gameRequest);
                break;
            }
            case MULTIUSER: {
                gameResponse = GameWithUserLogic.play(gameRequest);
                break;
            }
        }
        return gameResponse;
    }
}
