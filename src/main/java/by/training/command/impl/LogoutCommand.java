package by.training.command.impl;

import by.training.command.ActionCommand;
import by.training.entity.response.ResponseInfo;
import by.training.entity.response.ResponseType;

import javax.servlet.http.HttpServletRequest;

/**
 * Class provides realisation
 *
 * @author Maretskaya Anhelina
 * @see ActionCommand
 */
public class LogoutCommand implements ActionCommand {
    private static final String NEXT_PAGE = "/index.jsp";

    /**
     * Invalidate user session
     *
     * @param request - request from client to get parameters to work with
     * @return {@link ResponseInfo}
     */
    @Override
    public ResponseInfo execute(HttpServletRequest request) {
        ResponseInfo responseInfo = new ResponseInfo(NEXT_PAGE, ResponseType.REDIRECT);
        request.getSession().invalidate();
        return responseInfo;
    }
}
