package by.training.command.impl;

import by.training.command.ActionCommand;
import by.training.entity.response.ResponseInfo;
import by.training.entity.response.ResponseType;
import by.training.exception.CommandException;
import by.training.logic.profile.ShowStatisticsLogic;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Class provides game statistics for registered users.
 *
 * @author Maretskaya Anhelina
 * @see ActionCommand
 */
public class ShowStatisticsCommand implements ActionCommand {
    private static final String PARAM_NAME_USER_ID = "userId";
    private static final String PARAM_NAME_GAME = "game";
    private static final String PARAM_NAME_WIN_GAME = "winGame";
    private static final String SUCCESS_PAGE = "/jsp/common/statistics.jsp";
    private static final String FAIL_PAGE = "/jsp/common/main.jsp";
    private static final String PARAM_NAME_ERROR_STATISTICS = "errorMainMessage";
    private static final String PARAM_NAME_ERROR_MESSAGE = "try_later";
    public static final Logger LOGGER = Logger.getLogger(ShowStatisticsCommand.class);

    /**
     * Count all user games, and all winner games
     *
     * @param request - request from client to get parameters to work with
     * @return {@link ResponseInfo}
     */
    @Override
    public ResponseInfo execute(HttpServletRequest request) {
        ResponseInfo responseInfo = new ResponseInfo(SUCCESS_PAGE, ResponseType.FORWARD);
        int userId = (int) request.getSession().getAttribute(PARAM_NAME_USER_ID);
        try {
            ArrayList<Integer> list = ShowStatisticsLogic.show(userId);
            request.setAttribute(PARAM_NAME_WIN_GAME, list.get(0));
            request.setAttribute(PARAM_NAME_GAME, list.get(1));
        } catch (CommandException e) {
            responseInfo.setNextPage(FAIL_PAGE);
            request.setAttribute(PARAM_NAME_ERROR_STATISTICS, PARAM_NAME_ERROR_MESSAGE);
            LOGGER.error("can't count games");
        }
        return responseInfo;
    }
}
