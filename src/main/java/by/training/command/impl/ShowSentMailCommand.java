package by.training.command.impl;

import by.training.command.ActionCommand;
import by.training.entity.mail.Message;
import by.training.entity.response.ResponseInfo;
import by.training.entity.response.ResponseType;
import by.training.exception.CommandException;
import by.training.logic.mail.ShowSentMailLogic;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Class provides user to see sent mail.
 *
 * @author Maretskaya Anhelina
 * @see ActionCommand
 */
public class ShowSentMailCommand implements ActionCommand {
    private static final String PARAM_NAME_SENT_MAIL = "sentMailList";
    private static final String PARAM_NAME_LOGIN = "username";
    private static final String NEXT_PAGE = "/jsp/common/mail/sentMail.jsp";
    private static final String PARAM_NAME_ERROR_MESSAGE = "errorMailMessage";
    private static final String PARAM_NAME_ERROR_TEXT = "try_later";

    /**
     * Get all sent message by user login
     *
     * @param request - request from client to get parameters to work with
     * @return {@link ResponseInfo}
     */
    @Override
    public ResponseInfo execute(HttpServletRequest request) {
        ResponseInfo responseInfo = new ResponseInfo(NEXT_PAGE, ResponseType.FORWARD);
        String login = (String) request.getSession().getAttribute(PARAM_NAME_LOGIN);
        try {
            ArrayList<Message> messageList = ShowSentMailLogic.showSentMail(login);
            request.setAttribute(PARAM_NAME_SENT_MAIL, messageList);
        } catch (CommandException e) {
            request.setAttribute(PARAM_NAME_ERROR_MESSAGE, PARAM_NAME_ERROR_TEXT);
        }
        return responseInfo;
    }
}
