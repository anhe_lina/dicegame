package by.training.command.impl;

import by.training.command.ActionCommand;
import by.training.entity.profile.RoleType;
import by.training.entity.profile.User;
import by.training.entity.response.ResponseInfo;
import by.training.entity.response.ResponseType;
import by.training.exception.CommandException;
import by.training.logic.entry.SignUpLogic;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;

/**
 * Class provides new user registration
 *
 * @author Maretskaya Anhelina
 * @see ActionCommand
 */
public class SignUpCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_NAME_FIRST_NAME = "firstname";
    private static final String PARAM_NAME_lAST_NAME = "lastname";
    private static final String PARAM_NAME_EMAIL = "email";
    private static final String PARAM_NAME_BIRTHDAY = "birthday";
    private static final String PARAM_NAME_USERNAME = "username";
    private static final String PARAM_NAME_USER_ID = "userId";
    private static final String PARAM_NAME_USER_SCORE = "userScore";
    private static final String PARAM_NAME_USER_LOCALE = "userLocale";
    private static final String PARAM_NAME_LOCALE = "ru_RU";
    private static final String PARAM_NAME_PROFILE_IMAGE = "profileImg";
    private static final String PARAM_NAME_ROLE = "visitor";
    private static final String PARAM_NAME_ERROR_MESSAGE = "errorRegistrationMessage";
    private static final String SUCCESS_PAGE = "/jsp/common/main.jsp";
    private static final String FAIL_PAGE = "/jsp/guest/registration.jsp";
    public static final Logger LOGGER = Logger.getLogger(SignUpCommand.class);

    /**
     * Takes input parameters from {@link HttpServletRequest#getParameter(String)} and create {@link User} object
     * and passes parameters further to the Logic layer..
     * If {@link CommandException} is caught, then attribute {@link #PARAM_NAME_ERROR_MESSAGE} adds to
     * {@link HttpServletRequest#setAttribute(String, Object)} and navigates to {@link #FAIL_PAGE}.
     * If Logic operation passed successfully navigates to {@link #SUCCESS_PAGE}, else adds
     * {@link #PARAM_NAME_ERROR_MESSAGE} attribute to {@link HttpServletRequest#setAttribute(String, Object)}
     * and navigates to {@link #FAIL_PAGE}.
     * User role stores in session parameter {@link #PARAM_NAME_ROLE}.
     *
     * @param request - request from client to get parameters to work with
     * @return {@link ResponseInfo} with response parameters: next page and response type data for {@link by.training.servlet.DiceServlet})
     * @see SignUpLogic#addNewUser(User, String)
     */
    @Override
    public ResponseInfo execute(HttpServletRequest request) {
        ResponseInfo responseInfo = new ResponseInfo();
        User user = new User();
        user.setLogin(request.getParameter(PARAM_NAME_LOGIN));
        user.setRegistrationDate(LocalDate.now());
        String pass = request.getParameter(PARAM_NAME_PASSWORD);
        user.setFirstName(request.getParameter(PARAM_NAME_FIRST_NAME));
        user.setLastName(request.getParameter(PARAM_NAME_lAST_NAME));
        user.setBirthdate(LocalDate.parse(request.getParameter(PARAM_NAME_BIRTHDAY)));
        user.setEmail(request.getParameter(PARAM_NAME_EMAIL));
        try {
            int userId = SignUpLogic.addNewUser(user, pass);
            HttpSession session = request.getSession(true);
            session.setAttribute(PARAM_NAME_USERNAME, user.getLogin());
            session.setAttribute(PARAM_NAME_ROLE, RoleType.USER);
            session.setAttribute(PARAM_NAME_PROFILE_IMAGE, user.getAvatar());
            session.setAttribute(PARAM_NAME_USER_SCORE, user.getScore());
            session.setAttribute(PARAM_NAME_USER_ID, userId);
            if (session.getAttribute(PARAM_NAME_USER_LOCALE) == null) {
                session.setAttribute(PARAM_NAME_USER_LOCALE, PARAM_NAME_LOCALE);
            }
            responseInfo.setType(ResponseType.REDIRECT);
            responseInfo.setNextPage(SUCCESS_PAGE);
        } catch (CommandException e) {
            request.setAttribute(PARAM_NAME_ERROR_MESSAGE, e.getMessage());
            responseInfo.setType(ResponseType.FORWARD);
            responseInfo.setNextPage(FAIL_PAGE);
        }
        return responseInfo;
    }
}
