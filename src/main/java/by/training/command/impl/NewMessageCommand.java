package by.training.command.impl;

import by.training.command.ActionCommand;
import by.training.entity.mail.Message;
import by.training.entity.response.ResponseInfo;
import by.training.entity.response.ResponseType;
import by.training.exception.CommandException;
import by.training.logic.mail.NewMessageLogic;

import javax.servlet.http.HttpServletRequest;

/**
 * Class provides user to send messages to other users.
 *
 * @author Maretskaya Anhelina
 * @see ActionCommand
 */
public class NewMessageCommand implements ActionCommand {
    private static final String PARAM_NAME_RECIPIENT_LOGIN = "recipientLogin";
    private static final String PARAM_NAME_THEME = "theme";
    private static final String PARAM_NAME_LOGIN = "username";
    private static final String PARAM_NAME_TEXT_MESSAGE = "textMessage";
    private static final String NEXT_PAGE = "/jsp/common/mail/newMail.jsp";
    private static final String PARAM_NAME_ERROR_MESSAGE = "errorNewMessage";
    private static final String PARAM_NAME_ERROR_TEXT = "message_not_send";

    /**
     * Send message by login to another user
     *
     * @param request - request from client to get parameters to work with
     * @return {@link ResponseInfo}
     */
    @Override
    public ResponseInfo execute(HttpServletRequest request) {
        ResponseInfo responseInfo = new ResponseInfo();
        Message message = new Message();
        message.setRecipientLogin(request.getParameter(PARAM_NAME_RECIPIENT_LOGIN));
        message.setTheme(request.getParameter(PARAM_NAME_THEME));
        message.setText(request.getParameter(PARAM_NAME_TEXT_MESSAGE));
        message.setSenderLogin((String) request.getSession(true).getAttribute(PARAM_NAME_LOGIN));
        responseInfo.setNextPage(NEXT_PAGE);
        try {
            NewMessageLogic.sendMessage(message);
            responseInfo.setType(ResponseType.REDIRECT);
        } catch (CommandException e) {
            request.setAttribute(PARAM_NAME_ERROR_MESSAGE, PARAM_NAME_ERROR_TEXT);
            responseInfo.setType(ResponseType.FORWARD);
        }
        return responseInfo;
    }
}
