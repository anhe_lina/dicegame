package by.training.command.impl;

import by.training.command.ActionCommand;
import by.training.entity.profile.User;
import by.training.entity.response.ResponseInfo;
import by.training.entity.response.ResponseType;
import by.training.exception.CommandException;
import by.training.logic.profile.EditProfileLogic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Class provides user to make changes in profile.
 *
 * @author Maretskaya Anhelina
 * @see ActionCommand
 */
public class EditProfileCommand implements ActionCommand {
    private static final String PARAM_NAME_CARD_NUMBER = "crCard";
    private static final String PARAM_NAME_FIRST_NAME = "fnameEd";
    private static final String PARAM_NAME_lAST_NAME = "lnameEd";
    private static final String PARAM_NAME_LOGIN = "username";
    private static final String SUCCESS_PAGE = "/jsp/common/profileMain.jsp";
    private static final String FAIL_PAGE = "/jsp/common/main.jsp";
    private static final String PARAM_NAME_ERROR_MESSAGE = "errorMainMessage";
    private static final String PARAM_NAME_NO_CHANGES_SAVED = "no_changes_saved";

    /**
     * Changes {@link #PARAM_NAME_FIRST_NAME}, {@link #PARAM_NAME_lAST_NAME}, {@link #PARAM_NAME_CARD_NUMBER}
     * in user profile
     *
     * @param request - request from client to get parameters to work with
     * @return {@link ResponseInfo}
     */
    @Override
    public ResponseInfo execute(HttpServletRequest request) {
        ResponseInfo responseInfo = new ResponseInfo();
        HttpSession session = request.getSession(true);
        User user = new User();
        user.setFirstName(request.getParameter(PARAM_NAME_FIRST_NAME));
        user.setLastName(request.getParameter(PARAM_NAME_lAST_NAME));
        user.setCardNumber(request.getParameter(PARAM_NAME_CARD_NUMBER));
        user.setLogin((String) session.getAttribute(PARAM_NAME_LOGIN));
        try{
            EditProfileLogic.editProfile(user);
            responseInfo.setNextPage(SUCCESS_PAGE);
            responseInfo.setType(ResponseType.REDIRECT);
        }catch (CommandException e){
            responseInfo.setNextPage(FAIL_PAGE);
            responseInfo.setType(ResponseType.FORWARD);
            request.setAttribute(PARAM_NAME_ERROR_MESSAGE, PARAM_NAME_NO_CHANGES_SAVED);
        }
        return responseInfo;
    }
}
