package by.training.command.impl;

import by.training.command.ActionCommand;
import by.training.entity.response.ResponseInfo;
import by.training.entity.response.ResponseType;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Class provides user Russian and English UI.
 *
 * @author Maretskaya Anhelina
 * @see ActionCommand
 */
public class LocaleCommand implements ActionCommand {
    private static final String LOCALE = "locale";
    private static final String NEXT_PAGE = "/index.jsp";
    private static final String PARAM_NAME_USER_LOCALE = "userLocale";
    public static final Logger LOGGER = Logger.getLogger(LocaleCommand.class);

    /**
     *
     * @param request - request from client to get parameters to work with
     * @return {@link ResponseInfo}
     */
    @Override
    public ResponseInfo execute(HttpServletRequest request) {
        ResponseInfo responseInfo = new ResponseInfo(NEXT_PAGE, ResponseType.REDIRECT);
        String locale = request.getParameter(LOCALE);
        LOGGER.info(LOCALE + locale);
        request.getSession().setAttribute(PARAM_NAME_USER_LOCALE, locale);
        return responseInfo;
    }
}
