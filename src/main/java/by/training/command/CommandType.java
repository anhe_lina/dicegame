package by.training.command;

import by.training.command.impl.*;

/**
 * Enumeration of all available commands
 *
 * @author Maretskaya Anhelina
 */
public enum CommandType {
    SIGNIN {
        {
            this.command = new SignInCommand();
        }
    },
    SIGNUP {
        {
            this.command = new SignUpCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    SHOWPROFILE {
        {
            this.command = new ShowProfileCommand();
        }
    },
    EDITPROFILE {
        {
            this.command = new EditProfileCommand();
        }
    },
    LOAD {
        {
            this.command = new LoadCommand();
        }
    },
    REPLENISH {
        {
            this.command = new ReplenishAccountCommand();
        }
    },
    SHOWCREDITACCOUNT {
        {
            this.command = new ShowCreditAccountCommand();
        }
    },
    LOCALE {
        {
            this.command = new LocaleCommand();
        }
    },
    MONEYSETTING {
        {
            this.command = new MoneySettingCommand();
        }
    },
    NEWMESSAGE {
        {
            this.command = new NewMessageCommand();
        }
    },
    SHOWINBOX {
        {
            this.command = new ShowInboxCommand();
        }
    },
    SHOWSTATISTICS {
        {
            this.command = new ShowStatisticsCommand();
        }
    },
    SHOWSENTMAIL {
        {
            this.command = new ShowSentMailCommand();
        }
    };
    ActionCommand command;

    /**
     * Getter of current {@link #command}
     *
     * @return {@link ActionCommand}
     */
    public ActionCommand getCommand() {
        return command;
    }
}
