package by.training.command;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Define and create command from {@link CommandType}
 *
 * @author Maretskaya Anhelina
 */
public class ActionFactory {

    public static final Logger LOGGER = Logger.getLogger(ActionFactory.class);
    /**
     * Name of hidden input field with command name
     */
    private static final String PARAM_COMMAND = "command";

    /**
     * @param request - request from client to get parameters to work with
     * @return current {@link ActionCommand}
     */
    public ActionCommand defineCommand(HttpServletRequest request) {
        String action;
        action = request.getParameter(PARAM_COMMAND);
        LOGGER.info(PARAM_COMMAND + action);
        CommandType currentEnum = CommandType.valueOf(action.toUpperCase());
        return currentEnum.getCommand();
    }
}
