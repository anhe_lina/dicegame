package by.training.dao;

import by.training.connection.ProxyConnection;
import by.training.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.SQLException;

/**
 * The class provides root common abstraction for DAO layer classes.
 *
 * @author Maretskaya Anhelina
 */
public interface EntityDAO<T> {
    Logger LOGGER = Logger.getLogger(EntityDAO.class);

    /**
     *
     * @param entity is object extends {@link by.training.entity.Entity}
     * @throws DAOException if {@link SQLException} occurred while working with database
     * @see ProxyConnection
     * @see by.training.connection.ConnectionPool
     */
    void update(T entity) throws DAOException;

    /**
     * @param connection - add connection {@link ProxyConnection} back to {@link by.training.connection.ConnectionPool}
     */
    default void closeConnection(ProxyConnection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            LOGGER.error("SQL exception - can't close connection: " + e);
        }
    }
}
