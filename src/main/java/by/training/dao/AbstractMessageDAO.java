package by.training.dao;

import by.training.entity.mail.Message;
import by.training.exception.DAOException;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Class provides mail functionality
 *
 * @author Maretskaya Anhelina
 */
public abstract class AbstractMessageDAO {

    /**
     * @param message
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract void sendMessage(Message message) throws DAOException;

    /**
     * @param login
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract ArrayList<Message> findInbox(String login) throws DAOException;

    /**
     * @param login
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract ArrayList<Message> findSentMail(String login) throws DAOException;
}
