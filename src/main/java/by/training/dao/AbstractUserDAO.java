package by.training.dao;

import by.training.entity.profile.RoleType;
import by.training.entity.profile.User;
import by.training.exception.DAOException;

import java.math.BigDecimal;
import java.sql.SQLException;

/**
 * The class provides DAO abstraction for {@link User} objects.
 *
 * @author Maretskaya Anhelina
 * @see EntityDAO
 */
public abstract class AbstractUserDAO {
    /**
     * @param login
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract User findUserByLogin(String login) throws DAOException;

    /**
     * @param user
     * @param password
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract int create(User user, String password) throws DAOException;

    /**
     * Find password for user by login. Password is encrypted by MD5 encryptor.
     *
     * @param login String, input by user
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract String verifyPassword(String login) throws DAOException;

    /**
     * Check user access rights
     *
     * @param login
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract RoleType checkRole(String login) throws DAOException;

    /**
     * Save new avatar
     *
     * @param fileName
     * @param login
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract void saveAvatar(String fileName, String login) throws DAOException;

    /**
     * Change user money account balance
     *
     * @param login
     * @param score
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract void changeScore(String login, BigDecimal score) throws DAOException;

    /**
     * Verify credit card number
     *
     * @param login
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract User verifyCreditCard(String login) throws DAOException;

    /**
     * Count user games: winner = true - successful games
     *
     * @param userId
     * @param winner
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract int countGame(int userId, boolean winner) throws DAOException;
}
