package by.training.dao;

import by.training.entity.game.Game;
import by.training.entity.game.GameAccount;
import by.training.exception.DAOException;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Class provides game functionality
 *
 * @author Maretskaya Anhelina
 */
public abstract class AbstractGameDAO {

    /**
     * Create new game
     *
     * @param game
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract int createGame(Game game) throws DAOException;

    /**
     * Add player to the game
     *
     * @param gameAccount
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract void addPlayer(GameAccount gameAccount) throws DAOException;

    /**
     * Find game by id
     *
     * @param gameId
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract Game findGame(int gameId) throws DAOException;

    /**
     * @param gameId
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract GameAccount findPlayer(int gameId) throws DAOException;

    /**
     * Check user balance in case user can afford placed bet
     *
     * @param userId
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract BigDecimal checkBalance(int userId) throws DAOException;

    /**
     * Change user money balance
     *
     * @param userId
     * @param score
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract void updateScore(int userId, BigDecimal score) throws DAOException;

    /**
     * Update game max score and completeness
     *
     * @param gameId
     * @param maxScore
     * @param complete
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract void updateGame(int gameId, int maxScore, boolean complete) throws DAOException;

    /**
     * Mark all winner 'isWinner' true
     *
     * @param gameId
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract void markWinner(int gameId) throws DAOException;

    /**
     * Find available multi user games
     *
     * @param userId
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract ArrayList<GameAccount> incompleteGameList(int userId) throws DAOException;
}
