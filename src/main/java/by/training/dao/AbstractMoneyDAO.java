package by.training.dao;

import by.training.entity.profile.MoneyAccount;
import by.training.exception.DAOException;

import java.sql.SQLException;

/**
 * Class provides admin functionality to work with game settings
 *
 * @author Maretskaya Anhelina
 */
public abstract class AbstractMoneyDAO {

    /**
     * Get actual conversion rates
     *
     * @return
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract MoneyAccount findMoneyAccount() throws DAOException;

    /**
     * Change actual conversion rates
     *
     * @param moneyAccount
     * @throws DAOException if {@link SQLException} occurred while working with database
     */
    public abstract void changeMoneyAccount(MoneyAccount moneyAccount) throws DAOException;
}
