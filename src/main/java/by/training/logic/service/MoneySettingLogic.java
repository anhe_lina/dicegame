package by.training.logic.service;

import by.training.dao.MoneyDAO;
import by.training.entity.profile.MoneyAccount;
import by.training.exception.CommandException;
import by.training.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.SQLException;

/**
 * Class provides admin to change conversion rates.
 *
 * @author Maretskaya Anhelina
 */
public class MoneySettingLogic {
    public static final Logger LOGGER = Logger.getLogger(MoneySettingLogic.class);

    /**
     * Admin available. Change conversion rates.
     *
     * @param moneyAccount {@link MoneyAccount}
     * @throws CommandException if {@link SQLException} occurred while working with database
     */
    public static void fixMoney(MoneyAccount moneyAccount) throws CommandException {
        try {
            MoneyDAO moneyDAO = new MoneyDAO();
            moneyDAO.changeMoneyAccount(moneyAccount);
        } catch (DAOException e) {
            throw new CommandException("Can not update money settings.");
        }
    }
}
