package by.training.logic.mail;

import by.training.dao.MessageDAO;
import by.training.entity.mail.Message;
import by.training.exception.CommandException;
import by.training.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.SQLException;

/**
 * Class provides Logic layer for {@link by.training.command.impl.NewMessageCommand}
 *
 * @author Maretskaya Anhelina
 */
public class NewMessageLogic {
    public static final Logger LOGGER = Logger.getLogger(NewMessageLogic.class);

    /**
     * Pass {@link Message} to DAO layer
     *
     * @param message {@link Message}
     * @throws CommandException if {@link SQLException} occurred while working with database
     */
    public static void sendMessage(Message message) throws CommandException {
        try {
            MessageDAO messageDAO = new MessageDAO();
            messageDAO.sendMessage(message);
        } catch (DAOException e) {
            throw new CommandException("Can not send mail.");
        }

    }
}



