package by.training.logic.entry;

import by.training.dao.UserDAO;
import by.training.entity.profile.RoleType;
import by.training.exception.CommandException;
import by.training.exception.DAOException;

import java.sql.SQLException;

/**
 * Class provides Logic layer for getting user {@link RoleType}.
 *
 * @author Maretskaya Anhelina
 */
public class RoleLogic {
    private static final String PARAM_NAME_NO_USER_FOUND = "can_not_define_access_rights";

    /**
     * Verifies user role.
     *
     * @param login input by user
     * @return {@link RoleType} object for input login
     * @throws CommandException if {@link SQLException} occurred while working with database
     */
    public static RoleType checkRole(String login) throws CommandException {
        try {
            UserDAO userDAO = new UserDAO();
            return userDAO.checkRole(login);
        } catch (DAOException e) {
            throw new CommandException(PARAM_NAME_NO_USER_FOUND);
        }
    }
}
