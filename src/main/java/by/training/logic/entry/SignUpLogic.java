package by.training.logic.entry;

import by.training.dao.UserDAO;
import by.training.entity.profile.User;
import by.training.exception.CommandException;
import by.training.exception.DAOException;
import by.training.validation.impl.LoginValidation;
import by.training.validation.impl.NameValidation;
import by.training.validation.impl.PasswordValidation;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

/**
 * Class provides Logic layer for new user registration
 *
 * @author Maretskaya Anhelina
 */
public class SignUpLogic {
    private static final String PARAM_NAME_WRONG_NAME = "name_validation_error";
    private static final String PARAM_NAME_WRONG_PASSWORD = "password_validation_error";
    private static final String PARAM_NAME_WRONG_LOGIN = "login_validation_error";
    private static final String PARAM_NAME_EXISTING_LOGIN = "login_exist_error";
    public static final Logger LOGGER = Logger.getLogger(SignUpLogic.class);

    /**
     * Provides player registration operation. Calls DAO layer to insert all given and other needed data.
     *
     * @param user     - contains user information {@link User}
     * @param password - users' password
     * @return user ID
     * @throws CommandException if {@link DAOException} occurred while working with database or validation fails with
     * the relevant message
     * @see LoginValidation
     * @see PasswordValidation
     * @see NameValidation
     * @see UserDAO
     */
    public static int addNewUser(User user, String password) throws CommandException {
        LoginValidation loginValidation = new LoginValidation();
        PasswordValidation passwordValidation = new PasswordValidation();
        NameValidation nameValidation = new NameValidation();
        try {
            if (loginValidation.validate(user.getLogin())) {
                if (passwordValidation.validate(password)) {
                    if (nameValidation.validate(user.getFirstName()) && nameValidation.validate(user.getLastName())) {
                        UserDAO userDAO = new UserDAO();
                        return userDAO.create(user, DigestUtils.md5Hex(password));
                    } else {
                        throw new CommandException(PARAM_NAME_WRONG_NAME);
                    }
                } else {
                    throw new CommandException(PARAM_NAME_WRONG_PASSWORD);
                }
            } else {
                throw new CommandException(PARAM_NAME_WRONG_LOGIN);
            }
        } catch (DAOException e) {
            throw new CommandException(PARAM_NAME_EXISTING_LOGIN);
        }
    }
}
