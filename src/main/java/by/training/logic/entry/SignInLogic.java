package by.training.logic.entry;

import by.training.dao.UserDAO;
import by.training.entity.profile.User;
import by.training.exception.CommandException;
import by.training.exception.DAOException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

/**
 * Class provides Logic layer for new user authentication.
 *
 * @author Maretskaya Anhelina
 */
public class SignInLogic {
    private static final String PARAM_NAME_NO_USER_FOUND = "sign_in_no_user_found";
    private static final String PARAM_NAME_WRONG_PASSWORD = "sign_in_wrong_password";
    public static final Logger LOGGER = Logger.getLogger(SignInLogic.class);

    /**
     * Provides player authentication operation. Calls DAO layer to get all necessary user information.
     *
     * @param login    input by user
     * @param password input by user
     * @return {@link User} with information about user
     * @throws CommandException if {@link DAOException} occurred while working with database or passwords do not
     *                          match  with the relevant message
     * @see DAOException
     */
    public static User checkLogin(String login, String password) throws CommandException {
        UserDAO userDAO = new UserDAO();
        try {
            String pswd = userDAO.verifyPassword(login);
            if (DigestUtils.md5Hex(password).equals(pswd)) {
                return userDAO.findUserByLogin(login);
            }
        } catch (DAOException e) {
            throw new CommandException(PARAM_NAME_NO_USER_FOUND);
        }
        throw new CommandException(PARAM_NAME_WRONG_PASSWORD);
    }
}
