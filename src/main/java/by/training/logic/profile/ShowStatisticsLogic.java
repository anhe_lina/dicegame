package by.training.logic.profile;

import by.training.dao.UserDAO;
import by.training.exception.CommandException;
import by.training.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Class provides game statistics for registered users.
 *
 * @author Maretskaya Anhelina
 */
public class ShowStatisticsLogic {
    public static final Logger LOGGER = Logger.getLogger(ShowStatisticsLogic.class);

    /**
     *
     * @param userId parameter is taken from session
     * @return list with number of all games and number of winner games.
     * @throws CommandException f {@link SQLException} occurred while working with database
     */
    public static ArrayList<Integer> show(int userId) throws CommandException {
        try {
            UserDAO userDAO = new UserDAO();
            ArrayList<Integer> list = new ArrayList<>();
            int win = userDAO.countGame(userId, true);
            int fail = userDAO.countGame(userId, false);
            list.add(win);
            list.add(win + fail);
            return list;
        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
