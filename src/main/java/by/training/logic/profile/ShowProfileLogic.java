package by.training.logic.profile;

import by.training.dao.UserDAO;
import by.training.entity.profile.User;
import by.training.exception.CommandException;
import by.training.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.SQLException;

/**
 * Class provides user to see personal information.
 *
 * @author Maretskaya Anhelina
 */
public class ShowProfileLogic {

    public static final Logger LOGGER = Logger.getLogger(ShowProfileLogic.class);

    /**
     *
     * @param login input by user
     * @return {@link User} information
     * @throws CommandException if {@link SQLException} occurred while working with database
     */
    public static User getProfile(String login) throws CommandException {
        try {
            UserDAO userDAO = new UserDAO();
            return userDAO.findUserByLogin(login);
        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
