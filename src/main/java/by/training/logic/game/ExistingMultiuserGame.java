package by.training.logic.game;

import by.training.action.CountMaxScore;
import by.training.action.GameMechanics;
import by.training.connection.ConnectionPool;
import by.training.connection.ProxyConnection;
import by.training.dao.GameDAO;
import by.training.entity.game.Game;
import by.training.entity.game.GameAccount;
import by.training.entity.game.GameRequest;
import by.training.entity.game.GameResponse;
import by.training.exception.CommandException;
import by.training.exception.DAOException;
import by.training.validation.RateValidation;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;

/**
 * Logic for existing multi user game
 *
 * @author Maretskaya Anhelina
 */
public class ExistingMultiuserGame {
    private static final int PLAYER_NUMBER = 2;
    public static final Logger LOGGER = Logger.getLogger(ExistingMultiuserGame.class);

    public static GameResponse playExistingGame(GameRequest gameRequest) throws CommandException {
        ProxyConnection connection = ConnectionPool.getConnection();
        try {
            GameResponse gameResponse = new GameResponse();
            gameResponse.setConsignment(GameMechanics.findAllConsignment());
            gameResponse.setMaxScore(CountMaxScore.countScore(gameResponse.getConsignment()));
            gameResponse.setGameId(gameRequest.getGameId());

            connection.setAutoCommit(false);
            GameAccount gameAccount = new GameAccount();
            GameDAO gameDAO = new GameDAO(connection);
            Game game = gameDAO.findGame(gameRequest.getGameId());

            if (game.isComplete()) {
                throw new CommandException("game is complete yet");
            }
            gameAccount.setRate(game.getBank().divide(new BigDecimal(PLAYER_NUMBER), RoundingMode.UP));
            BigDecimal balance = gameDAO.checkBalance(gameRequest.getUserId());
            if (RateValidation.checkInadmissibility(gameAccount.getRate(), balance)) {
                throw new CommandException("not enough money");
            }

            GameAccount competitorAccount = gameDAO.findPlayer(gameResponse.getGameId());
            gameResponse.setOpponentMaxScore(competitorAccount.getUserScore());

            gameAccount.setUserId(gameRequest.getUserId());
            gameAccount.setUserScore(gameResponse.getMaxScore());
            gameAccount.setGameId(game.getGameId());
            if (gameResponse.getMaxScore() > game.getMaxScore()) {
                //второй выиграл
                gameAccount.setWinner(true);
                gameDAO.updateGame(game.getGameId(), gameResponse.getMaxScore(), true);
                gameDAO.updateScore(gameRequest.getUserId(), gameAccount.getRate());
                gameDAO.addPlayer(gameAccount);
            } else {
                if (gameResponse.getMaxScore() == game.getMaxScore()) {
                    //ничья
                    gameAccount.setWinner(true);
                    gameDAO.updateScore(competitorAccount.getUserId(), gameAccount.getRate());

                } else {
                    //второй проиграл
                    gameDAO.updateScore(gameRequest.getUserId(), gameAccount.getRate().negate());
                    gameDAO.updateScore(competitorAccount.getUserId(), game.getBank());
                }
                gameDAO.updateGame(game.getGameId(), competitorAccount.getUserScore(), true);
                gameDAO.addPlayer(gameAccount);
            }
            gameDAO.markWinner(game.getGameId());
            connection.commit();
            return gameResponse;
        } catch (DAOException | SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                LOGGER.error(ex.getMessage());
            }
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e.getMessage());
            }
        }
        throw new CommandException("can't save the game");
    }
}
