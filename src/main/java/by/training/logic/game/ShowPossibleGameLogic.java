package by.training.logic.game;

import by.training.connection.ConnectionPool;
import by.training.connection.ProxyConnection;
import by.training.dao.GameDAO;
import by.training.entity.game.GameAccount;
import by.training.exception.CommandException;
import by.training.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Show user all available multi user games
 *
 * @author Maretskaya Anhelina
 */
public class ShowPossibleGameLogic {
    public static final Logger LOGGER = Logger.getLogger(ShowPossibleGameLogic.class);

    public static ArrayList<GameAccount> show(int userId) throws CommandException {
        ProxyConnection connection = ConnectionPool.getConnection();
        try {
            connection.setAutoCommit(false);
            GameDAO gameDAO = new GameDAO(connection);
            ArrayList<GameAccount> list = gameDAO.incompleteGameList(userId);
            connection.commit();
            return list;
        } catch (DAOException | SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                LOGGER.error(ex.getMessage());
            }
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e.getMessage());
            }
        }
        throw new CommandException("can't get available games");
    }
}
