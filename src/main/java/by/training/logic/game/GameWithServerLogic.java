package by.training.logic.game;

import by.training.action.CountMaxScore;
import by.training.action.GameMechanics;
import by.training.connection.ConnectionPool;
import by.training.connection.ProxyConnection;
import by.training.dao.GameDAO;
import by.training.entity.game.*;
import by.training.exception.CommandException;
import by.training.exception.DAOException;
import by.training.validation.RateValidation;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;

/**
 * Logic for game with server
 *
 * @author Maretskaya Anhelina
 */
public class GameWithServerLogic {

    private static final int PLAYER_NUMBER = 2;
    public static final Logger LOGGER = Logger.getLogger(GameWithServerLogic.class);

    public static GameResponse play(GameRequest gameRequest) throws CommandException {
        ProxyConnection connection = ConnectionPool.getConnection();
        try {
            GameResponse gameResponse = new GameResponse();
            gameResponse.setOpponentConsignment(GameMechanics.findAllConsignment());
            gameResponse.setConsignment(GameMechanics.findAllConsignment());
            gameResponse.setOpponentMaxScore(CountMaxScore.countScore(gameResponse.getOpponentConsignment()));
            gameResponse.setMaxScore(CountMaxScore.countScore(gameResponse.getConsignment()));
            connection.setAutoCommit(false);
            GameDAO gameDAO = new GameDAO(connection);
            BigDecimal balance = gameDAO.checkBalance(gameRequest.getUserId());
            if(RateValidation.checkInadmissibility(gameRequest.getRate(), balance)) {
                throw new CommandException("not enough money");
            }
            gameResponse.setUserScore(balance.subtract(gameRequest.getRate()));
            gameDAO.updateScore(gameRequest.getUserId(), gameRequest.getRate().negate());
            Game game = new Game();
            GameAccount gameAccount = new GameAccount();
            gameAccount.setUserId(gameRequest.getUserId());
            gameAccount.setRate(gameRequest.getRate());
            gameAccount.setUserScore(gameResponse.getMaxScore());
            game.setBank(gameRequest.getRate().multiply(new BigDecimal(PLAYER_NUMBER)).setScale(2, RoundingMode.DOWN));
            if (gameResponse.getMaxScore() >= gameResponse.getOpponentMaxScore()) {
                game.setMaxScore(gameResponse.getMaxScore());
                gameAccount.setWinner(true);
                if (gameResponse.getMaxScore() == gameResponse.getOpponentMaxScore()) {
                    game.setBank(gameRequest.getRate());
                }
            } else {
                game.setMaxScore(gameResponse.getOpponentMaxScore());
            }
            game.setComplete(true);
            int gameId = gameDAO.createGame(game);
            gameAccount.setGameId(gameId);
            gameDAO.addPlayer(gameAccount);
            gameResponse.setGameId(gameId);
            if(gameAccount.isWinner()){
                BigDecimal newBalance = gameResponse.getUserScore().add(game.getBank());
                gameDAO.updateScore(gameRequest.getUserId(), game.getBank());
                gameResponse.setUserScore(newBalance);
            }
            gameResponse.setWinner(gameResponse.getMaxScore() >= gameResponse.getOpponentMaxScore());
            connection.commit();
            return gameResponse;
        } catch (DAOException | SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                LOGGER.error(ex.getMessage());
            }
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.error(e.getMessage());
            }
        }
        throw new CommandException("can't save the game");
    }
}