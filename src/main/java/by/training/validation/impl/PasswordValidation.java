package by.training.validation.impl;

import by.training.validation.Validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The class implements method for rate validation.
 *
 * @author Maretskaya Anhelina
 */
public class PasswordValidation implements Validation {

    /**
     * Validation regular expression.
     */
    private static final String PASSWORD_REGEX = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,})";

    /**
     * @param password input by user
     * @return true if first\last name is valid
     */
    @Override
    public boolean validate(String password) {
        if (password != null) {
            Pattern pattern = Pattern.compile(PASSWORD_REGEX);
            Matcher matcher = pattern.matcher(password);
            return matcher.matches();
        }
        return false;
    }
}
