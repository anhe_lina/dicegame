package by.training.validation.impl;

import by.training.validation.Validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The class implements method for login validation.
 *
 * @author Maretskaya Anhelina
 */
public class LoginValidation implements Validation {
    /**
     * Validation regular expression.
     */
    private static final String LOGIN_REGEX = "^[a-zA-Zа-яА-Я0-9_-]{5,}$";

    /**
     * @param login input by user
     * @return true if login is valid
     */
    @Override
    public boolean validate(String login) {
        if (login != null) {
            Pattern pattern = Pattern.compile(LOGIN_REGEX);
            Matcher matcher = pattern.matcher(login);
            return matcher.matches();
        }
        return false;
    }
}
