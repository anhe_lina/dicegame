package by.training.validation.impl;

import by.training.validation.Validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The class implements method for credit card number validation.
 *
 * @author Maretskaya Anhelina
 */
public class CreditCardValidation implements Validation {
    /**
     * Validation regular expression.
     */
    private static final String CREDIT_CARD_REGEX = "(\\d){16}";

    /**
     * @param creditCardNumber input by user
     * @return true if creditCardNumber is valid
     */
    @Override
    public boolean validate(String creditCardNumber) {
        if (creditCardNumber != null) {
            Pattern pattern = Pattern.compile(CREDIT_CARD_REGEX);
            Matcher matcher = pattern.matcher(creditCardNumber);
            return matcher.matches();
        }
        return false;
    }
}
