package by.training.validation.impl;

import by.training.validation.Validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The class implements method for first\last name validation.
 *
 * @author Maretskaya Anhelina
 */
public class NameValidation implements Validation {
    /**
     * Validation regular expression.
     */
    private static final String NAME_REGEX = "((?!.*\\d)(?=.*[a-zA-Zа-яА-Я]).{2,})";

    /**
     * @param name input by user
     * @return true if first\last name is valid
     */
    @Override
    public boolean validate(String name) {
        if (name != null) {
            Pattern pattern = Pattern.compile(NAME_REGEX);
            Matcher matcher = pattern.matcher(name);
            return matcher.matches();
        }
        return false;
    }
}
