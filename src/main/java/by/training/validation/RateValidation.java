package by.training.validation;

import java.math.BigDecimal;

/**
 * The class for rate validation.
 *
 * @author Maretskaya Anhelina
 */
public class RateValidation {

    /**
     * Checks if user has enough money to place input rate.
     *
     * @param rate        input by user
     * @param actualScore users' money balance
     * @return true if actualScore equals or greater than rate
     */
    public static boolean checkInadmissibility(BigDecimal rate, BigDecimal actualScore) {
        return (rate != null) && (actualScore != null) && actualScore.subtract(rate).signum() < 0;
    }
}
