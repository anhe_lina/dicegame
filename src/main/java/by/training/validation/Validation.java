package by.training.validation;

/**
 * The class provides abstract method for all validation classes.
 *
 * @author Maretskaya Anhelina
 */
public interface Validation {

    /**
     * @param parameter input by user
     * @return true if parameter is valid: matches regular expression or game business logic.
     */
    boolean validate(String parameter);
}