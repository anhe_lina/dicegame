package by.training.action;

import java.util.ArrayList;
import java.util.List;

/**
 * Count user final score in the game
 *
 * @author Maretskaya Anhelina
 */
public class CountMaxScore {

    private static final int FIRST_WIHTDRAW_NUMBER = 2;
    private static final int SECOND_WIHTDRAW_NUMBER = 5;

    /**
     * @param consignmentList consists of user throws, each throw consists of dice value
     * @return user game score
     */
    public static int countScore(ArrayList<List<Integer>> consignmentList) {
        int score = 0;
        for (List<Integer> list : consignmentList) {
            if (noWithdrawNumber(list)) {
                score += list.stream().reduce(0, Integer::sum);
            }
        }
        return score;
    }

    /**
     * @param list of generated numbers
     * @return boolean: if the sum of values adds to user score
     * According to the rules, if there are no '2' or '5' return true
     */
    private static boolean noWithdrawNumber(List<Integer> list) {
        return list.stream().filter(o -> (o == FIRST_WIHTDRAW_NUMBER) || (o == SECOND_WIHTDRAW_NUMBER)).count() == 0;
    }
}
