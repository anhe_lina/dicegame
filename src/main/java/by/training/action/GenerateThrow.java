package by.training.action;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Generate dice values for one throw
 *
 * @author Maretskaya Anhelina
 */
class GenerateThrow {

    private static final int MAX_NUMBER = 7;
    private static final int MIN_NUMBER = 1;

    /**
     * @param k number of available dice
     * @return list of k generated values
     */
    static List<Integer> generate(int k) {
        IntStream stream = IntStream.generate(() -> ThreadLocalRandom.current().nextInt(MIN_NUMBER, MAX_NUMBER)).limit(k);
        return stream.boxed().collect(Collectors.toList());
    }
}