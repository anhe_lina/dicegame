package by.training.validation;

import by.training.validation.impl.LoginValidation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Maretskaya Anhelina
 */
public class LoginValidationTest {
    @Before
    public void init() {
    }

    @Test
    public void validateTestValidCheck() {
        LoginValidation loginValidation = new LoginValidation();
        String validLogin = "Ange_123_лина";
        Assert.assertTrue(loginValidation.validate(validLogin));
    }

    @Test
    public void validateTestInvalidCheck() {
        LoginValidation loginValidation = new LoginValidation();
        String invalidLogin = " Ange_123_лина";
        Assert.assertFalse(loginValidation.validate(invalidLogin));
    }
}