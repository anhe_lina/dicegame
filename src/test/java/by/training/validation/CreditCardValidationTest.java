package by.training.validation;

import by.training.validation.impl.CreditCardValidation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Maretskaya Anhelina
 */
public class CreditCardValidationTest {

    @Before
    public void init() {
    }

    @Test
    public void validateTestValidCheck() {
        CreditCardValidation creditCardValidation = new CreditCardValidation();
        String validCardNumber = "1234567890123456";
        Assert.assertTrue(creditCardValidation.validate(validCardNumber));
    }

    @Test
    public void validateTestInvalidCheck() {
        CreditCardValidation creditCardValidation = new CreditCardValidation();
        String invalidCardNumber = "09876543210987654";
        Assert.assertFalse(creditCardValidation.validate(invalidCardNumber));
    }
}