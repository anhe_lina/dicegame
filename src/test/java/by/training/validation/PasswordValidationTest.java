package by.training.validation;

import by.training.validation.impl.PasswordValidation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Maretskaya Anhelina
 */
public class PasswordValidationTest {

    @Before
    public void init() {
    }

    @Test
    public void validateTestValidCheck() {
        PasswordValidation passwordValidation = new PasswordValidation();
        String validPassword = "1qWяаE_RTy";
        Assert.assertTrue(passwordValidation.validate(validPassword));
    }

    @Test
    public void validateTestInvalidCheck() {
        PasswordValidation passwordValidation = new PasswordValidation();
        String invalidPassword = "12WT_Y234";
        Assert.assertFalse(passwordValidation.validate(invalidPassword));
    }
}