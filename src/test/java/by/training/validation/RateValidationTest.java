package by.training.validation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * @author Maretskaya Anhelina
 */
public class RateValidationTest {

    public static BigDecimal rate;

    @Before
    public void init() {
        rate = new BigDecimal("10.00");
    }

    @Test
    public void validateTestLessCheck() {
        BigDecimal lessActualScore = new BigDecimal("9.99");
        Assert.assertTrue(RateValidation.checkInadmissibility(rate, lessActualScore));
    }

    @Test
    public void validateTestGreaterCheck() {
        BigDecimal greaterActualScore = new BigDecimal("10.99");
        Assert.assertFalse(RateValidation.checkInadmissibility(rate, greaterActualScore));
    }

    @Test
    public void validateTestEqualsCheck() {
        BigDecimal equalsActualScore = new BigDecimal("10.00");
        Assert.assertFalse(RateValidation.checkInadmissibility(rate, equalsActualScore));
    }
}