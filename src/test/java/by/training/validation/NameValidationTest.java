package by.training.validation;

import by.training.validation.impl.NameValidation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Maretskaya Anhelina
 */
public class NameValidationTest {
    @Before
    public void init() {
    }

    @Test
    public void validateTestValidCheck() {
       NameValidation nameValidation = new NameValidation();
        String validName = "Ange_лина";
        Assert.assertTrue(nameValidation.validate(validName));
    }

    @Test
    public void validateTestInvalidCheck() {
        NameValidation nameValidation = new NameValidation();
        String invalidName = " Ange123лина";
        Assert.assertFalse(nameValidation.validate(invalidName));
    }
}