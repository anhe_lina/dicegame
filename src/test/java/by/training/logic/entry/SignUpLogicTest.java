package by.training.logic.entry;

import by.training.entity.profile.User;
import by.training.exception.CommandException;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

/**
 * @author Maretskaya Anhelina
 */
public class SignUpLogicTest {
    private User user;
    private String password;
    private static final int USER_ID = 45;
    private static final String LOGIN = "Anhelina";
    private static final String CARD_NUMBER = "1234567890123456";
    private static final String FIRST_NAME = "Ангелина";
    private static final String LAST_NAME = "Марецкая";
    private static final String EMAIL = "example@mail.com";
    private static final LocalDate BIRTH_DATE;

    static {
        BIRTH_DATE = LocalDate.parse("1997-01-27");
    }

    @Before
    public void init() {
        user = new User();
        password = "123Qwerty";
        user.setBirthdate(BIRTH_DATE);
        user.setEmail(EMAIL);
        user.setLogin(LOGIN);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setCardNumber(CARD_NUMBER);
    }

    @Test(expected = CommandException.class)
    public void addNewUserTestCheckInvalidPassword() throws CommandException {
        String invalidPassword = "123qwerty";
        SignUpLogic.addNewUser(user, invalidPassword);
    }

    @Test(expected = CommandException.class)
    public void addNewUserTestCheckInvalidName() throws CommandException {
        String invalidName = "Winner1";
        user.setFirstName(invalidName);
        SignUpLogic.addNewUser(user, password);
    }

    @Test(expected = CommandException.class)
    public void addNewUserTestCheckInvalidLogin() throws CommandException {
        String invalidLogin = "Я";
        user.setLogin(invalidLogin);
        SignUpLogic.addNewUser(user, password);
    }

}